# dvd2PureAudio

Extract audio tracks from video DVD as high definition audio files (WAV, FLAC or AC3).

Video DVD have a much greater storage capacity than audio CD so that they can
offer much more multimedia data (both video and audio) than audio CD. As a consequence,
audio tracks on video DVD are usually recorded with a higher sampling rate
(48 kHz instead of 44 kHz) and they may be simultaneously available in
different encoding formats (stereo/surround). Then it's a good idea to listen video
DVD on a High-Fidelity system, because the sound quality would be better.
Plus, video DVD generally offer live concerts that may not be available on audio
CD.

## SYNOPSIS

`dvd2PureAudio DVD_INPUT TITLE# FIRST_TRACK# LAST_TRACK# [AUDIO#]`

Extract DVD chapters (FIRST#..LAST#) from DVD title TITLE# and audio stream AUDIO# as audio files

or

`dvd2PureAudio DVD_INPUT`

Display DVD content (available titles, chapters and audio streams)

## DESCRIPTION

`dvd2PureAudio` is a Linux shell BASH script that extracts audio tracks from video DVD
as WAV, AC3 or FLAC files to play them on computer audio players
(banshee, audirvana, ...) rather than on TV DVD players.

>dvd2PureAudio can not handle audio CD, DVD-AUDIO nor Bluray discs : it's only usable on
DVD video content (discs or files).

### Command line arguments short description:
- `DVD_INPUT`: a file or a device from where the DVD content can be read
- `TITLE#`: the title index that contains the tracks/chapters to extract
- `FIRST_TRACK#`: the index of the first track/chapter to extract from the DVD
- `LAST_TRACK#`: the index of the last track/chapter to extract from the DVD
- `AUDIO#` (optional parameter, default value=1): the index of the audio stream (PCM, AC3) to use

### `DVD_INPUT`
A file (.iso) or a device (/dev/dvd for example) from where the DVD content can be read.
When extracting from a hardware device, the script reads each track several times.
It stops when the size of the read bytes is equal to
the same value a predefined number of times (`HIT` variable). A pause of few seconds
(`HW_PAUSE` variable) is also applied after each track extraction to prevent hardware heating.
As a matter of fact, reading from a DVD device can sometimes be hazardous : if the hardware is old or
of poor quality or over-heating, the read bytes may be different from one trial to the other!
These controls are not applied when extraction is performed from a .iso DVD content.

### `TITLE#`
The title index that contains the tracks/chapters to extract.
A DVD is composed of several titles that contain in turn several chapters. Usually,
for a live band concert, a title correspond to a live session, and each chapter
correspond to a song.
If you want to extract tracks from several titles, you must call the script once
for each title.

### `FIRST_TRACK#`
The index of the first track/chapter to extract from the DVD.

### `LAST_TRACK#`
The index of the last track/chapter to extract from the DVD.

### `AUDIO#`
The index of the audio stream (PCM, AC3) to use.
The `AUDIO#` argument is optional but should be carefully chosen because DVD generally
have several audio streams available (stereo PCM, stereo AC3, 5.1 AC3, ...) and the resulting
audio files have different audio qualities. Preferences should be given to stereo PCM
or stereo AC3. 5.1 AC3 audio streams are *not* usable. In order to display the available
audio streams on a DVD, run `dvd2PureAudio DVD_INPUT` where DVD_INPUT is a link
to your DVD device or your DVD .iso.

## OUTPUT FILES

### File types

dvd2PureAudio is producing FLAC files but also WAV or AC3 files depending on the audio streams encoding format:
*  Stereo PCM DVD audio streams generate WAV and FLAC files
*  Stereo AC3 DVD audio streams generate AC3 and FLAC files

For each generated tracks, there is also a LOG file with the output of the toolchain
used to generate the file (transcode and ffmpeg).

The toolchain also generates .tmp files but they should be deleted after normal completion.

The files are generated in the current working directory. You may want to create
a new folder for each DVD you are using.

AC3 audio files may be listened on some advanced audio players like [Banshee](http://banshee.fm/)

### File names conventions

When the files are generated the DVD layout is converted using the following alignment:

| DVD layout | Audio files layout |
| ------ | ------ |
| Title | disc |
| Chapter | track |

Then the generated file names have the following structure : `discDD-trackTT.EXT`
*  `DD` is the Title/Disc number
*  `TT` is the Chapter/Track number
*  `EXT` is the file extension : .log, .wav , .ac3 or .flac

### Basic tagging & FLAC metadata

FLAC files are tagged with the following metadata:

| Metadata | Value |
| ------ | ------ |
| album | the DVD main title (which is not the name of the TITLE# !) |
| discnumber | the title number (`TITLE#`) |
| tracknumber | the track number (`FIRST_TRACK#` .. `LAST_TRACK#`)|

### Full FLAC metadata configuration

Before importing the audio FLAC files in your audio library you may complete the
metadata information with a dedicated software like [MusicBrainz Picard](https://picard.musicbrainz.org)

## INSTALLATION

dvd2PureAudio needs some additional software that should be installed
on your Linux distribution before use:
- libdvdread + libdvdcss
- [lsdvd](https://manpages.ubuntu.com/manpages/trusty/man1/lsdvd.1.html)
- [xpath](https://manpages.ubuntu.com/manpages/xenial/en/man1/xpath.1p.html)
- [transcode](https://manpages.ubuntu.com/manpages/trusty/man1/transcode.1.html)
- [ffmpeg](https://manpages.ubuntu.com/manpages/xenial/en/man1/ffmpeg.1.html)

* xpath: copyright 2000 Fastnet Software Ltd.
* lsdvd: written by Chris Philips.  <acid_kewpie@users.sourceforge.net>
* transcode: Written by Thomas Oestreich <ostreich@theorie.physik.uni-goettingen.de>, Tilmann Bitterberg and the Transcode-Team
* ffmpeg: Copyright (c) 2000-2015 the FFmpeg developers


dvd2PureAudio is a Linux shell (BASH) script, therefore it needs execution rigths to
execute:

~~~bash
chmod u+x dvd2PureAudio
~~~

If you want to grant access on `dvd2PureAudio` to all users on your computer you
may copy it into `/usr/local/bin` (as the root user):

~~~bash
sudo cp /path/to/local/downloads/dvd2PureAudio /usr/local/bin
sudo chmod a+x /usr/local/bin/dvd2PureAudio
~~~

## SUPPORTED PLATFORMS

dvd2PureAudio has been tested on Ubuntu 14.04.6 LTS (trusty) with the following packages:

| Package | Version |
| ------ | ------ |
| bash |  4.3-7ubuntu1.7 |
| lsdvd | 0.16-4 |
| xpath (libxml-xpath-perl) | 2.0108+dfsg-1ubuntu0.2 |
| transcode | 3:1.1.7-8 |
| ffmpeg | 7:2.7.2-1dhor~trusty |

dvd2PureAudio (V1.0.3) has been tested on CentOS 7 with the following packages:

| Package | Version |
| ------ | ------ |
| bash | 4.2.46-33.el7 |
| lsdvd | 0.16-18.el7.nux |
| xpath (perl-XML-XPath) | 1.13-22.el7 |
| transcode | 1.1.7-17.el7 |
| ffmpeg | 1.1.7-17.el7 |

dvd2PureAudio (V1.0.4) has been tested on ArchLinux 5.8.14-arch1-1 with the following packages:

| Package | Version |
| ------ | ------ |
| bash | 5.0.018-1 |
| lsdvd | 0.17-4 |
| xpath (perl-xml-xpath) | 1.44-3 |
| transcode | 1.1.7-34 |
| ffmpeg | 2:4.3.1-2 |

> A serious bug has been discovered and fixed during ArchLinux portage: the output messages from 'libdvdread' are sent to the standard stream output by 'tccat'. As a result, the audio stream is corrupted and can not be decoded by the other modules of the 'transcode' toolchain. The unwanted lines are now discarded with the 'sed' command.


## EXAMPLES OF USE

### Example 1 : AC3/FLAC audio files extraction

Each video DVD has its own specific titles/chapters/audio layout and therefore it's a
good idea to start by displaying the content of any DVD you want to use. To do that,
just run `dvd2PureAudio` with the name of the DVD device where your DVD is loaded:

~~~
$ dvd2PureAudio /dev/sr1
dvd2PureAudio> Displaying DVD '/dev/sr1' content with 'lsdvd' :
Disc Title: XXXXXXX
Title: 01, Length: 02:29:51.600 Chapters: 25, Cells: 25, Audio streams: 03, Subpictures: 00
	Audio: 1, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 2, AP: 0, Content: Undefined, Stream id: 0x80
	Audio: 2, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x81
	Audio: 3, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x8a

Title: 02, Length: 00:18:55.367 Chapters: 05, Cells: 05, Audio streams: 03, Subpictures: 00
	Audio: 1, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 2, AP: 0, Content: Undefined, Stream id: 0x80
	Audio: 2, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x81
	Audio: 3, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x8a

Longest track: 01


dvd2PureAudio> Bye !
$
~~~
We can see that this video DVD (a live concert) has a rather simple layout:
*  Title #1 contains the main session with 25 chapters, that may correspond to the live set list (1 chapter per song)
*  Title #2 contains some "bonus" tracks

There are 3 audio streams per title:
*  "Audio: 1" = AC3 stereo (2 channels)
*  "Audio: 2" = AC3 5.1 (6 channels)
*  "Audio: 3" = DTS 5.1 (6 channels)

The only audio stream that can be usable for pure audio listening sessions is the "Audio: 1" stream, because it is AC3/stereo encoded.

The next step is to extract the tracks from the main session (id. Title #1).
In this example, we only extract the first 3 tracks of the Title #1, using the audio stream #1:

~~~
$ dvd2PureAudio /dev/sr1 1 1 3 1

dvd2PureAudio> User parameters:
DVD INPUT             = '/dev/sr1'
TITLE #               = '1'
FIRST TRACK #         = '1'
LAST TRACK #          = '3'
AUDIO TRACK #         = '1'
DVD TITLE             = 'XXXXXXX'

dvd2PureAudio> Audio parameters:
AUDIO FORMAT          = 'ac3'
AUDIO FREQUENCY       = '48000'
AUDIO QUANTIZATION    = 'drc'
AUDIO CHANNELS        = '2'

dvd2PureAudio> Extraction parameters (/track):
Max extraction trials = '6'
Nb needed hits        = '1'
Pause (seconds)       = '4'

dvd2PureAudio> Starting to extract audio from DVD '/dev/sr1':
__%__ ___track___ _phase_ ________file________ _____________status_____________
01/03 [=>.......] extract disc01-track01_1.tmp fmt=ac3 size=2283520b -REF-
|~~~~ [==>......] extract disc01-track01_2.tmp fmt=ac3 size=2283520b >HIT< 1/1
|~~~~ [=======>.]  rename disc01-track01.ac3   done.
|~~~~ [========>]  encode disc01-track01.flac  done.
02/03 [=>.......] extract disc01-track02_1.tmp fmt=ac3 size=10047584b -REF-
|~~~~ [==>......] extract disc01-track02_2.tmp fmt=ac3 size=10047584b >HIT< 1/1
|~~~~ [=======>.]  rename disc01-track02.ac3   done.
|~~~~ [========>]  encode disc01-track02.flac  done.
03/03 [=>.......] extract disc01-track03_1.tmp fmt=ac3 size=8880256b -REF-
|~~~~ [==>......] extract disc01-track03_2.tmp fmt=ac3 size=8880256b >HIT< 1/1
|~~~~ [=======>.]  rename disc01-track03.ac3   done.
|~~~~ [========>]  encode disc01-track03.flac  done.

dvd2PureAudio> Audio extraction completed. Bye !
$
~~~

Let's see what we get:
~~~
$ ls -lh *.ac3
-rw-rw-r-- 1 xxxxx xxxxx 2,2M nov.  11 18:14 disc01-track01.ac3
-rw-rw-r-- 1 xxxxx xxxxx 9,6M nov.  11 18:15 disc01-track02.ac3
-rw-rw-r-- 1 xxxxx xxxxx 8,5M nov.  11 18:16 disc01-track03.ac3
$ ls -lh *.flac
-rw-rw-r-- 1 xxxxx xxxxx 14M nov.  11 18:14 disc01-track01.flac
-rw-rw-r-- 1 xxxxx xxxxx 67M nov.  11 18:15 disc01-track02.flac
-rw-rw-r-- 1 xxxxx xxxxx 59M nov.  11 18:16 disc01-track03.flac
$
~~~

>You may notice how much the FLAC files are bigger in size than the AC3 ones.
That's because the FLAC files are generated with a sample size (quantization) of 24 bits by ffmpeg !

### Example 2 : WAV/FLAC audio files extraction

>In order to separate generated files, you may want to create a new folder for each new DVD.

As usual, we first need to have an idea of the layout of this second DVD:
~~~
$ dvd2PureAudio /dev/sr1

dvd2PureAudio> User parameters:
DVD INPUT             = '/dev/sr1'

dvd2PureAudio> Displaying DVD '/dev/sr1' content with 'lsdvd' :
Disc Title: YYYYYYYY
Title: 01, Length: 01:06:23.500 Chapters: 19, Cells: 20, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 02, Length: 01:06:23.500 Chapters: 19, Cells: 20, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 03, Length: 01:06:23.500 Chapters: 19, Cells: 20, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 04, Length: 01:06:23.500 Chapters: 19, Cells: 20, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 05, Length: 00:03:16.133 Chapters: 02, Cells: 02, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 06, Length: 00:02:03.367 Chapters: 02, Cells: 02, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 07, Length: 00:01:21.100 Chapters: 02, Cells: 02, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 08, Length: 00:03:09.800 Chapters: 02, Cells: 02, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 09, Length: 00:00:00.500 Chapters: 01, Cells: 01, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 10, Length: 00:00:00.500 Chapters: 01, Cells: 01, Audio streams: 03, Subpictures: 02
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0
	Audio: 2, Language: en - English, Format: dts, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x89
	Audio: 3, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x82

Title: 11, Length: 00:46:27.000 Chapters: 06, Cells: 06, Audio streams: 01, Subpictures: 05
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0

Title: 12, Length: 00:46:26.500 Chapters: 05, Cells: 05, Audio streams: 01, Subpictures: 05
	Audio: 1, Language: en - English, Format: lpcm , Frequency: 48000, Quantization: 16bit, Channels: 2, AP: 0, Content: Undefined, Stream id: 0xa0

Longest track: 01


dvd2PureAudio> Bye !
~~~

This DVD layout is a little more complex than the previous one. We have:
*  Titles #1 to #4 seems identical (same length, same number of chapters, 3 audio streams)...
*  Titles #5 to #8 seems to be individual "bonus" tracks (only 2 chapters)
*  Titles #9 & #10 are of zero length (!)
*  Titles #11 & #12 seems *almost* identical, with only one audio stream

>Without the (physical) DVD table of content it can be hard to figure out what's really inside...

In this example, we have no other choice than to use Audio stream #1, because it's the only 2 channels (stereo) stream.
Then let's extract the first 3 tracks, from Title 2, of this audio stream:
~~~
$ dvd2PureAudio /dev/sr1 2 1 3 1

dvd2PureAudio> User parameters:
DVD INPUT             = '/dev/sr1'
TITLE #               = '2'
FIRST TRACK #         = '1'
LAST TRACK #          = '3'
AUDIO TRACK #         = '1'
DVD TITLE             = 'YYYYYYYY'

dvd2PureAudio> Audio parameters:
AUDIO FORMAT          = 'lpcm'
AUDIO FREQUENCY       = '48000'
AUDIO QUANTIZATION    = '16bit'
AUDIO CHANNELS        = '2'

dvd2PureAudio> Extraction parameters (/track):
Max extraction trials = '6'
Nb needed hits        = '1'
Pause (seconds)       = '4'

dvd2PureAudio> Starting to extract audio from DVD '/dev/sr1':
__%__ ___track___ _phase_ ________file________ _____________status_____________
01/03 [=>.......] extract disc02-track01_1.tmp fmt=pcm size=37041280b -REF-
|~~~~ [==>......] extract disc02-track01_2.tmp fmt=pcm size=37041280b >HIT< 1/1
|~~~~ [=======>.]   write disc02-track01.wav   done.
|~~~~ [========>]  encode disc02-track01.flac  done.
02/03 [=>.......] extract disc02-track02_1.tmp fmt=pcm size=41739992b -REF-
|~~~~ [==>......] extract disc02-track02_2.tmp fmt=pcm size=41739992b >HIT< 1/1
|~~~~ [=======>.]   write disc02-track02.wav   done.
|~~~~ [========>]  encode disc02-track02.flac  done.
03/03 [=>.......] extract disc02-track03_1.tmp fmt=pcm size=38376792b -REF-
|~~~~ [==>......] extract disc02-track03_2.tmp fmt=pcm size=38376792b >HIT< 1/1
|~~~~ [=======>.]   write disc02-track03.wav   done.
|~~~~ [========>]  encode disc02-track03.flac  done.

dvd2PureAudio> Audio extraction completed. Bye !
~~~

Let's check the results:
~~~
$ ls -lh *.wav
-rw-rw-r-- 1 xxxxx xxxxx 36M nov.  11 19:15 disc02-track01.wav
-rw-rw-r-- 1 xxxxx xxxxx 40M nov.  11 19:15 disc02-track02.wav
-rw-rw-r-- 1 xxxxx xxxxx 37M nov.  11 19:16 disc02-track03.wav
$ ls -lh *.flac
-rw-rw-r-- 1 xxxxx xxxxx 25M nov.  11 19:15 disc02-track01.flac
-rw-rw-r-- 1 xxxxx xxxxx 26M nov.  11 19:15 disc02-track02.flac
-rw-rw-r-- 1 xxxxx xxxxx 24M nov.  11 19:16 disc02-track03.flac
~~~

>In this second example FLAC files are of smaller size than the WAV ones.

>You may also reuse the WAV files to burn back an audio CD with a selection of the tracks,
if you want to listen them on your "good old" Hi-Fi stereo system ;-)

## AUTHOR

Written by Eric Yape : Eric DOT Yape AT gmail DOT com

## COPYRIGHT

Copyright 2019 by Eric Yape
